import express from "express";
import { deleteComment, reply } from "../../services/comment";
import { commentOwner, loggedInUser } from "../validators";

const commentsV1 = express.Router();


// List Comments
commentsV1.get('/', (req, res) => {
    console.log('Listing comments');
    res.json({ message: 'Successfully Called' });
});


// Reply
commentsV1.post('/:id/reply', loggedInUser, (req, res) => {
  reply(req.body['post_id'], {
    text: req.body['text'],
    parent: req.params['id'],
    owner: req.headers['username'] as string
  })
    .then(result => res.status(200).json(result))
    .catch(err => { console.log(err); res.status(422).json({}); });
});


// Edit Comment
commentsV1.put('/:id', loggedInUser, commentOwner, (req, res) => {
    console.log('Editing comments');
    res.json({ message: 'Successfully Called' });
});


// Delete Comment
commentsV1.delete('/:id', loggedInUser, commentOwner, (req, res) => {
  deleteComment( req.body['post_id'], req.params['id'])
    .then(result => res.status(200).json({}))
    .catch(err => res.status(422).json(err));

});


export default commentsV1;
