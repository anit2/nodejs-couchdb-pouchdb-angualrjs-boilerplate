import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import makeRoutes from './routes';
import { initCouchDb } from './dbs/couch';


// TODO: Create and import all handlers

export const app: express.Application = express();

app.use(cors());
app.use(bodyParser.json());


initCouchDb()

makeRoutes(app)

const port = parseInt(process.env.PORT || '') || 3000;

app.listen(port, "0.0.0.0", () => {
  console.log(`Server Started at PORT: ${port}`);
});
