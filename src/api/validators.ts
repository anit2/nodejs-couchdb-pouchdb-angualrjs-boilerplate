import { body, validationResult } from 'express-validator';
import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import { getComment } from '../services/comment';
import { getPost } from '../services/post';


export const userRegisterRules = () => {
  return [
    body('username').isLength({ min: 1 }),
    body('password').isLength({ min: 5 })
  ]
};

export const loggedInUser = (req: Request, res: Response, next: NextFunction) => {
  const token = req.headers['authtoken'] as string;
  console.log('token is ', token, req.headers)
  try {
    const user = jwt.verify(token, process.env['secret'] || 'insecure_secret_123') as any;
    req.headers['username'] = user.username;
    next();
  } catch(e) {
    res.status(401).json({ message: 'Unauthorized' })
  }
}


export const commentOwner = (req: Request, res: Response, next: NextFunction) => {
  getComment(req.body['post_id'], req.params.id).then((result: any) => {
    if (result.owner === req.headers['username']) next()
    else res.status(401).json({ message: 'Unauthorized' });
 });
}

export const postOwner = (req: Request, res: Response, next: NextFunction) => {
  getPost(req.params['id']).then((result: any) => {
    if (result.owner === req.headers['username']) next()
    else res.status(401).json({ message: 'Unauthorized' });
 });
}

export const validate = (req: Request, res: Response, next: NextFunction) => {
  const errors = validationResult(req);
  return (
      errors.isEmpty()
        ? next()
        : res.status(422).json({
            errors: errors.array().map(err => ({ [err.param]: err.msg })),
        })
  );
};
