import express from "express";
import { registerUser, loginUser } from "../../services/user";
import { userRegisterRules, validate } from '../validators'
import { Request, Response } from 'express';
import { DocumentInsertResponse } from "nano";
import * as jwt from "jsonwebtoken";


const userV1 = express.Router();

const signUser = (username: string) => {
  return {
    username: username,
    token: jwt.sign(
      { username },
      process.env['secret'] || 'insecure_secret_123',
      { expiresIn: '7d' }
    )
  };

}


// New User Register
userV1.post(
    '/register',
    userRegisterRules(),
    validate,
    (req: Request, res: Response) => {
        registerUser(req.body)
          .then(result => res.status(200).json(signUser(result.id.split(':')[1])))
          .catch(err => res.status(422).json(err.json));
    }
);


// New Login
userV1.post('/session', (req, res) => {
  loginUser(req.body, (err, user) => {
    console.log('error is ', err)
    if (err) res.status(401).json({ messasge: 'Error' })
    else res.status(200).json(signUser(user.username));
  })
});

export default userV1;
