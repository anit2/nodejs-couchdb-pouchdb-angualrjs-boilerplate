import { MaybeDocument } from "nano";
import {couchDbNode} from "../dbs/couch"
import { Comment } from "./comment";


const postsDb = couchDbNode.use('posts');

export interface Post {
    title: string;
    link: string;
    owner: string;
}


export const addPost = async (post: Post) => {
  const { id } = await postsDb.insert(post as MaybeDocument);
  const {ok, error, reason} = await couchDbNode.db.create(`post_comments_${id}`);

  console.log('returned values are ', ok, error, reason);
  if (error) throw 'Not able to create DB';

  return { id };
};

export const getPost = (postId: string) => (
  postsDb.get(postId)
);

export const deletePost = (postId: string) => (
  postsDb.get(postId).then(result => postsDb.destroy(postId, result._rev))
)


export const postComment = async (postId: string, post: Comment) => await couchDbNode.use(`post_comments_${postId}`).insert(post as MaybeDocument);
