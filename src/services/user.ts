import nano, { MaybeDocument } from "nano";
import {couchDbNode} from "../dbs/couch"

const usersDb = couchDbNode.use('_users');

export interface NewUser {
    username: string;
    password: string;
}

const userDecorated = (user: NewUser) => ({
  ...user,
  name: user.username,
  type: 'user',
  roles: [],
  _id: `org.couchdb.user:${user.username}`
});

export const registerUser = (user: NewUser) => (
  usersDb
    .insert(userDecorated(user) as MaybeDocument)
);

export const loginUser = (user: NewUser, cb: (err?: any, user?: any) => any) => (
  nano({ url: 'http://localhost:5984' })
    .auth(user.username, user.password, (err, result, headers) => {
      if(err) return cb(err);

      cb(null, { username: result.name });
    })
);
