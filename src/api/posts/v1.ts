import express from "express";
import { addPost, postComment, deletePost } from "../../services/post";
import { loggedInUser, postOwner } from "../validators";

const postsV1 = express.Router();

// New Post
postsV1.post('/', loggedInUser, (req, res) => {
  addPost({ ...req.body, owner: req.headers['username'] })
    .then(result => res.status(200).json(result.id ))
    .catch(err => res.status(422).json(err));
});


// New Post
postsV1.post('/:id/comment', loggedInUser, (req, res) => {
  postComment(req.params['id'], { ...req.body, owner: req.headers['username'] })
    .then(result => res.status(200).json(result.id ))
    .catch(err => res.status(422).json(err));
});


// Delete Post
postsV1.delete('/:id', loggedInUser, postOwner, (req, res) => {
  deletePost(req.params['id'])
    .then(result => res.status(200).json({}))
    .catch(err => res.status(422).json(err));
});


export default postsV1;
