import { MaybeDocument } from "nano";
import {couchDbNode} from "../dbs/couch"

export interface Comment {
    text: string;
    parent?: string;
    owner: string;
}


export const reply = async (postId: string, comment: Comment) => {
  const { id } = await couchDbNode.db.use(`post_comments_${postId}`).insert(comment as MaybeDocument);
  return { id };
};

export const editComment = (postId: string, commentId: string, newText: string) => {
  // const db = couchDbNode.db.use(`post_comments_${postId}`);
  // return db.get(postId)
  //   .then(result => db.insert({ ...result, text: newText }))
};

export const getComment = async (postId: string, commentId: string) => (
  couchDbNode.db.use(`post_comments_${postId}`).get(commentId)
);

export const deleteComment = (postId: string, commentId: string) => {
  const db = couchDbNode.db.use(`post_comments_${postId}`);
  return db.get(postId)
    .then(result => db.destroy(commentId, result._rev))
}

