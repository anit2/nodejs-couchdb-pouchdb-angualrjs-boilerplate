import { Server } from 'http';
import nano, {ServerScope} from 'nano';


export const couchDbNode =  nano(process.env['COUCHDB_URL'] || 'http://admin:admin@0.0.0.0:5984');

// Create dbs if they dont exist;
export const initCouchDb = async () => {
    const { error: usersError } = await couchDbNode.db.create('users');
    console.log('user error is ', usersError)
    const { error: commentsError } = await couchDbNode.db.create('comments');
    console.log('comments error is  error is ', commentsError);

    return couchDbNode;
}
