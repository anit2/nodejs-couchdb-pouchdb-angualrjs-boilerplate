import express from 'express';
import userV1 from './api/user/v1';
import commentsV1 from './api/comments/v1';
import postsV1 from './api/posts/v1';

const makeRoutes = (app: express.Application) => {
    app.use('/v1/user', userV1);
    app.use('/v1/comments', commentsV1);
    app.use('/v1/posts', postsV1);
};

export default makeRoutes;
